package com.company;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    final static Logger logger = Logger.getLogger(Main.class);
    static String request;

    public static void main(String[] args) throws URISyntaxException {
        JSONParser parser = new JSONParser();
        Scanner sc = new Scanner(System.in);
        //TODO: Think of a way to pass it as argument in utility service
        //to segregate auto and property folder in resource
        System.out.println("Enter Request Type: 1.persauto 2.property");
        request = sc.nextLine();
        URL url = Main.class.getClassLoader().getResource("request/" + request);
        if (url == null) {
            logger.error("Directory Not Found");
        } else {
            logger.info("Reading all the files from--> " + request + " Directory");
            File filePath = Paths.get(url.toURI()).toFile();
            File[] listOfFiles = filePath.listFiles();
            //to store source-target
            Map<String, List<String>> mapping = new HashMap<>();
            for (File file : listOfFiles) {
                String fileName = file.getName();
                logger.info("Reading -->" + fileName);
                try (FileReader reader = new FileReader(file.getAbsolutePath())) {
                    Object obj = parser.parse(reader);
                    if (obj instanceof JSONArray) {
                        JSONArray result = (JSONArray) obj;
                        for (int i = 0; i < result.size(); i++) {
                            JSONObject objects = (JSONObject) result.get(i);
                            String sapiPath = (String) objects.get("source");
                            sapiPath = sapiPath.replace("*", "{ref}");
                            String ratingPath = (String) objects.get("target");
                            if (!mapping.containsKey(sapiPath)) {
                                mapping.put(sapiPath, new ArrayList<String>());
                            }
                            mapping.get(sapiPath).add(ratingPath);
                        }
                    } else {
                        JSONObject enumJson = (JSONObject) obj;
                        Iterator<String> enumKeys = enumJson.keySet().iterator();
                        while (enumKeys.hasNext()) {
                            String enumKey = enumKeys.next();
                            JSONObject node = (JSONObject) enumJson.get(enumKey);
                            Iterator<String> nodeKeys = node.keySet().iterator();
                            while (nodeKeys.hasNext()) {
                                String nodeKey = nodeKeys.next();
                                String nodeValue = String.valueOf(node.get(nodeKey));
                                if (!mapping.containsKey(enumKey + "/" + nodeKey)) {
                                    mapping.put(enumKey + "/" + nodeKey, new ArrayList<>());
                                }
                                mapping.get(enumKey + "/" + nodeKey).add(nodeValue);
                            }
                        }
                    }

                    //removing extension
                    fileName = fileName.substring(0, fileName.lastIndexOf('.'));
                    //storing into csv file
                    storeIntoCSV(mapping, fileName);
                    mapping.clear();
                } catch (FileNotFoundException e) {
                    logger.error("Failed File->" + fileName, e);
                } catch (IOException e) {
                    logger.error("Failed File->" + fileName, e);
                } catch (ParseException e) {
                    logger.error("Failed File->" + fileName, e);
                } catch (Exception e) {
                    logger.error("Failed File->" + fileName, e);
                }
            }
        }
    }

    /**
     * It stores SAPI->RATING KEY_VALUE PAIR into CSV file
     *
     * @param mapping : Hashmap of source:target value
     */
    private static void storeIntoCSV(Map<String, List<String>> mapping, String fileName) throws URISyntaxException {
        File dir = new File(System.getProperty("user.dir") + "/" + request + "mappings");
        if (!dir.exists()) {
            dir.mkdir();
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(dir.getAbsolutePath() + "/" + fileName + ".csv");
        } catch (FileNotFoundException e) {
            logger.error("Failed File-> " + fileName, e);
        }
        StringBuilder sb = new StringBuilder();
        String columnNameList = "SAPI,OE_DTO";
        sb.append(columnNameList + "\n");
        for (Map.Entry<String, List<String>> a : mapping.entrySet()) {
            for (String temp : a.getValue()) {
                sb.append(a.getKey() + "," + temp + "\n");
            }
        }
        pw.write(sb.toString());
        pw.close();
        logger.info("Stored into CSV");
    }
}
