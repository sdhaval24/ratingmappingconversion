### Mapping Validation Automation ###

#### Quick summary
* This is a simple java application that quickly generates CSV file from JSON file to provide SAPI -> Orch Engine Rating Mappings
#### Summary of set up
* Clone the project and open in you favorite choice of IDE.
#### How to run application
* mvn clean
* mvn install
* copy files and paste inside request/Auto or property Folder inside resources
* Run application
* provide folder Name for which you want to generate files 
* Corresponding CSV file be created inside auto or property mappings Folder